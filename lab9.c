#include<stdio.h>
void swapnum(int*num1,int*num2)
{
int tempnum;
tempnum=*num1;
*num1=*num2;
*num2= tempnum;
}
int main()
{
int num1= 35,num2=49;
printf("before swapping : ");
printf("\n num1 value is :%d",num1);
printf("\n num2 value is : %d\n",num2);
swapnum (&num1,&num2);
printf("after swapping :");
printf("\n num1 value is :%d",num1);
printf("\n num2 value is :%d",num2);
return 0;
}                                                                                                 